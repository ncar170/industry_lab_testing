package ictgradschool.industry.lab_testing.ex04;

import ictgradschool.Keyboard;

public class ShapeAreaCalculator {

    double circleArea;
    double rectangleArea;

    public void start(){

        System.out.println("Welcome to Shape Area Calculator");
        System.out.println();
        System.out.print("Enter the width of the rectangle: ");
        String sWidth = Keyboard.readInput();
        double width  = convertStringToDouble(sWidth);
        System.out.println();
        System.out.print("Enter the length of the rectangle: ");
        String sLength = Keyboard.readInput();
        double length = convertStringToDouble(sLength);
        System.out.println();
        System.out.print("Enter the radius of the circle: ");
        String sRadius = Keyboard.readInput();
        double radius = convertStringToDouble(sRadius);

        circleArea = calculateCircleArea(radius);
        rectangleArea = calculateRectangleArea(width, length);

        int circle = roundAreaToWholeNumber(circleArea);
        int rectangle = roundAreaToWholeNumber(rectangleArea);

        int smallArea = compareAreaSizes(circle, rectangle);

        System.out.println();
        System.out.println("The smaller area is: " + smallArea);

    }


    public double convertStringToDouble(String string){

        double d = 0.0;

        try {
            d = Double.parseDouble(string);
        } catch (NumberFormatException e){
            System.out.println("Incorrect data type entered");
        }
        return d;
    }

    public double calculateRectangleArea (double width, double length){

        rectangleArea = (width * length);

        return rectangleArea;
    }

    public double calculateCircleArea (double radius){

        circleArea = ((Math.PI) * (radius * radius));

        return circleArea;

    }

    public int roundAreaToWholeNumber (double area){

       return (int) Math.round(area);

    }

    public int compareAreaSizes (int circle, int rectangle) {

        if (circle > rectangle) {
            return rectangle;
        } else {
            return circle;
        }
    }

    public static void main(String[] args) {
        ShapeAreaCalculator s = new ShapeAreaCalculator();
        s.start();
    }


}
