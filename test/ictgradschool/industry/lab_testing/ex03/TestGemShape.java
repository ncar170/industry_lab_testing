package ictgradschool.industry.lab_testing.ex03;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestGemShape {

    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    @Test
    public void TestSmallGemShape(){
        GemShape shape = new GemShape(0, 0, 5, 5, 20, 20);

        shape.paint(painter);

        int[] xpoints = {0, 10, 20, 10};
        int[] ypoints = {10, 0 ,10, 20};

        assertEquals("(polygon xpoints: " + Arrays.toString(xpoints) + ", ypoints: " + Arrays.toString(ypoints) + ")", painter.toString());

    }

    @Test
    public void testLargeGemShape(){
        GemShape shape = new GemShape(10, 10, 5 , 5, 50 ,50);

        int[] xpoints = {0, 30, 40, 60, 40, 30};
        int[] ypoints = {35, 10 ,10, 35, 60, 60};


        shape.paint(painter);
        assertEquals("(polygon xpoints: " + Arrays.toString(xpoints) + ", ypoints: " + Arrays.toString(ypoints) + ")", painter.toString());
    }

    @Test
    public void testSmallShapeMove() {
       GemShape shape = new GemShape(0, 0, 15, 10, 20, 20);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        int[] xpoints = {0, 10, 20, 10};
        int[] ypoints = {10, 0 ,10, 20};

        int[] moveX = {15, 25, 35, 25};
        int[] moveY = {20, 10, 20 ,30};

        assertEquals("(polygon xpoints: " + Arrays.toString(xpoints) + ", ypoints: " + Arrays.toString(ypoints) + ")(polygon xpoints: " + Arrays.toString(moveX) + ", ypoints: " + Arrays.toString(moveY) + ")", painter.toString());

    }

    @Test
    public void testLargeShapeMove() {
        GemShape shape = new GemShape(10, 10, 15, 10, 50, 50);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        assertEquals("(polygon xpoints: [10, 30, 40, 60, 40, 30], ypoints: [35, 10, 10, 35, 60, 60])(polygon xpoints: [25, 45, 55, 75, 55, 45], ypoints: [45, 20, 20, 45, 70, 70])",
                painter.toString());
    }

}
