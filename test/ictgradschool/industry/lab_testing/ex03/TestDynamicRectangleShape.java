package ictgradschool.industry.lab_testing.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDynamicRectangleShape {

    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    @Test
    public void TestSimpleMove(){
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 20, 12, 15);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        assertEquals("(rectangle 100,20,25,35)(rectangle 112,35,25,35)",
                painter.toString());
    }

    @Test
    public void TestRightWallBounceUnFilled(){
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 20, 100, 20);
        shape.paint(painter);
        shape.move(200, 200);
        shape.paint(painter);

        assertEquals("(rectangle 100,20,25,35)(getColor 0,0,0)(setColor 0,0,0)(fillRect 175,40,25,35)(setColor 0,0,0)(rectangle 175,40,25,35)",
                painter.toString());
    }

    @Test
    public void TestRightWallBounceFilled(){
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 20, 100, 20);
        shape.paint(painter);
        shape.move(200, 200);
        shape.paint(painter);
        shape.move(200, 200);
        shape.paint(painter);
        shape.move(200, 200);

        assertEquals("(rectangle 100,20,25,35)(getColor 0,0,0)(setColor 0,0,0)(fillRect 175,40,25,35)(setColor 0,0,0)(rectangle 175,40,25,35)",
                painter.toString());
    }

}
