package ictgradschool.industry.lab_testing.ex02;

import ictgradschool.industry.lab_testing.ex01.Robot;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }

    private Dictionary myDictionary;

    @Before //This will be run, then the first test will be run. It will then be run again before the second test is run
    public void setUp() { //if it is @BeforeClass, then it will run once and then all the tests will run consecutively
        myDictionary = new Dictionary();
    }


    @Test
    public void testIsSpellingCorrectWithWord(){
       boolean correct = myDictionary.isSpellingCorrect("remember");
       assertTrue(correct);
    }

    @Test
    public void testIsSpellingCorrectWithoutWord(){
        boolean correct = myDictionary.isSpellingCorrect("remembeq");
        assertFalse(correct);
    }
}