package ictgradschool.industry.lab_testing.ex02;

import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    private SimpleSpellChecker checker;
    private SimpleSpellChecker checkerTwo;
    private Map<String, Integer> userWords;

    @Before //This will be run, then the first test will be run. It will then be run again before the second test is run
    public void setUp() throws InvalidDataFormatException { //if it is @BeforeClass, then it will run once and then all the tests will run consecutively
        checker = new SimpleSpellChecker(new Dictionary(), "the people remembeq people");
        checkerTwo = new SimpleSpellChecker(new Dictionary(), "the people remember");
    }

    @Test
    public void testGetMisspelledWordsPresent(){
        List<java.lang.String> strings = new ArrayList<>();
        strings.add("remembeq");
        assertEquals(strings, checker.getMisspelledWords());
    }

    @Test
    public void testGetMisspelledWordsAbsent(){
        List<String> strings = new ArrayList<>();
        assertEquals(strings, checkerTwo.getMisspelledWords());
    }

    @Test
    public void testGetUniqueWords(){
        List<java.lang.String> strings = new ArrayList<>();
        strings.add("the");
        strings.add("remember");
        strings.add("people");
        assertEquals(strings, checkerTwo.getUniqueWords());
    }

    @Test
    public void testFrequencyOfWords() throws InvalidDataFormatException {
        assertEquals(2, checker.getFrequencyOfWord("people"));
    }
}