package ictgradschool.industry.lab_testing.ex04;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

public class TestShapeAreaCalculator {

    private ShapeAreaCalculator shape;

    @Before
    public void setup(){
        shape = new ShapeAreaCalculator();
    }

    @Test
    public void TestConvertStringToDouble(){

         assertEquals(2.58, shape.convertStringToDouble("2.58"), 1e-15);
         
    }

    @Test
    public void TestRectangleArea(){

        assertEquals(20.0, shape.calculateRectangleArea(4, 5), 1e-15);

    }

    @Test
    public void TestCircleArea(){

        assertEquals(452.16, shape.calculateCircleArea(12), 1 );
    }

    @Test
    public void TestWholeNumbers(){

        assertEquals(452, shape.roundAreaToWholeNumber(452.16));

    }

    @Test
    public void TestCompareAreaSizes(){

        assertEquals(360, shape.compareAreaSizes(450, 360));
        assertEquals(400, shape.compareAreaSizes(400, 500));

    }



}
