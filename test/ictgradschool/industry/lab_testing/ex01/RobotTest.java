package ictgradschool.industry.lab_testing.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RobotTest {

    private Robot myRobot;

    @Before //This will be run, then the first test will be run. It will then be run again before the second test is run
    public void setUp() { //if it is @BeforeClass, then it will run once and then all the tests will run consecutively
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testTurnEast() {
        for (int i = 0; i < 1; i++) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }

    @Test
    public void testTurnSouth() {
        for (int i = 0; i < 2; i++) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }

    @Test
    public void testTurnWest() {
        for (int i = 0; i < 3; i++) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }

    @Test
    public void testTurnNorth() {
        for (int i = 0; i < 4; i++) {
            myRobot.turn();
        }
        assertEquals(Robot.Direction.North, myRobot.getDirection());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testMoveNorth() {
        try {
            myRobot.move();
            assertEquals(9, myRobot.currentState().row);
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testIllegalMoveEast() {
        for (int i = 0; i < 1; i++) {
            myRobot.turn();
        }
        boolean atRight = false;
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++) {
                myRobot.move();
            }
            atRight = myRobot.currentState().column == 10;
            assertTrue(atRight);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testMoveEast() {
        for (int i = 0; i < 1; i++) {
            myRobot.turn();
        }
        try {
            myRobot.move();
            assertEquals(2, myRobot.column());
        } catch (IllegalMoveException e) {
            fail();
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        for (int i = 0; i < 2; i++) {
            myRobot.turn();
        }
        boolean atBottom = false;
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 10; i++) {
                myRobot.move();
            }
                atBottom = myRobot.currentState().row == 10;
                assertTrue(atBottom);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testMoveSouth() {
        for (int i = 0; i < 2; i++) {
            myRobot.turn();
        }
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveWest(){
        for (int i = 0; i < 3; i++){
            myRobot.turn();
        }
        boolean atLeft = false;
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 10; i++){
                myRobot.move();
            }
            atLeft = myRobot.currentState().column == 1;
            assertTrue(atLeft);
        } catch (IllegalMoveException e){
            fail();
        }

        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e){
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testMoveWest() {
        for (int i = 0; i < 3; i++) {
            myRobot.turn();
        }
        try {
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testBackTrack(){
        try {
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++){
                myRobot.move();
            }
            assertEquals(1, myRobot.currentState().row);
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++){
                myRobot.backTrack();
            }
            assertEquals(10, myRobot.currentState().row);
        } catch (IllegalMoveException e) {
            fail();
        }

    }
}
